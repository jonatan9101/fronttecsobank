import React from "react";
import { Link } from "react-router-dom";

class NavBar extends React.Component {
  handleLogout() {
    localStorage.removeItem("token");
    console.log(localStorage.getItem("token"));
    //this.setState({ redirect: "/" });
  }

  render() {
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
            <Link className="navbar-brand" to="#">
              TECSO BANK
            </Link>
            <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
              <li className="nav-item">
                <Link className="nav-link" to="/accountowner">
                  Titulares<span className="sr-only"></span>
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/account">
                  Cuentas<span className="sr-only"></span>
                </Link>
              </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                <li className="nav-item active">
                  <Link to="/">
                    {" "}
                    <span className="nav-link" onClick={this.handleLogout}>
                      Cerrar sesión
                    </span>
                  </Link>
                </li>
              </ul>
            </form>
          </div>
        </nav>
        <br></br>
        {this.props.children}
      </React.Fragment>
    );
  }
}

export default NavBar;
