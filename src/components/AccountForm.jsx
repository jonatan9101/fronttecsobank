import React from "react";
import { Link } from "react-router-dom";


import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import TableAccount from "./TableAccount";

class AccountForm extends React.Component {
    render() {
        return <TableAccount accountsList={this.props.accountsList} 
        handleClickAddMovement={this.props.handleClickAddMovement} 
        handleClickDelete={this.props.handleClickDelete} 
        movementTypelist={this.props.movementTypelist} 
        createAddMovement={this.props.createAddMovement} 
        newMovement={this.props.newMovement} 
        handleClickMovement={this.props.handleClickMovement}
        handleClickView={this.props.handleClickView}
        ></TableAccount>;
    }
}
export default AccountForm;
