import React, { Component } from 'react';
import {Route, Redirect} from 'react-router-dom';
import decode from 'jwt-decode'
import PropTypes from 'prop-types'


function isAuthenticated  () {
    var isValid = true;
    try {
      var token = localStorage.getItem("token");
      isValid = false;
      console.log("Token es: " + token);
      isValid = decode(token);
    } catch (ex) {
      localStorage.removeItem("token");
      console.log("Error: " + ex);
      return false;
    }
    console.log("Valido:"+isValid)
    return isValid;
  };


  const PrivateRoute = ({ component: Component, ...rest }) => {
    return (
      <Route {...rest} render={
        props => 
        isAuthenticated() ? (
            <Component {...rest} {...props} />
            ):(
                <Redirect to="/"/>
            )
      } />
    )
  }

export default PrivateRoute;