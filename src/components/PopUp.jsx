import React from 'react';

import './styles/PopUp.css'
import { styles } from '@material-ui/pickers/views/Calendar/Calendar';

class PopUp extends React.Component {


    render() {
        
        if(this.props.content==undefined){
            return <div/>;
        }
        var id="popupBody";
        if(this.props.content.size){
            var id="popupBodyBig";
        }
        return (
            <div id="popup" class="overlay">
                <div id={id}>
                    <h2>{this.props.content.popupTitle}</h2>
                    <a id="cerrar" href="#" onClick={this.props.handleClickClear} >&times;</a>
                    <div class="popupContent">
                        {this.props.content.popupContent}
                    </div>
                </div>
            </div>
        )
    }
}

export default PopUp