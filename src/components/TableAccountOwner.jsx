import React from "react";

import api from "../api.js";
import "./styles/TableAccountOwner.css";

import add from "./img/plus.svg";
import del from "./img/delete.svg";
import edit from "./img/pencil.svg";

class TableAccountOwner extends React.Component {
  handleClick = async (accountOwner, e) => {
    try {
      console.log("Entro handdler");

      const data = await api.AccountOwner.delete(accountOwner.id);
      if (data.status == 204) {
        delete this.props.accountOwnersList[accountOwner];
      }
      console.log(this.props.accountOwnersList);
      this.setState({});
    } catch (ex) {
      this.setState({
        error: ex,
        loading: false,
      });
    }
  };

  render() {
    var data = JSON.parse(this.props.accountOwnersList);
    return (
      
      <div className="table__data">
        <br />
        <div className="row">
          <div className="col-md-12">
            <a href="#popup">
              <div>
                <button
                  onClick={this.props.handleCreate}
                  className="btn__add"
                  /* className="btn btn-primary float-right" */
                >
                  <label className="lbl__add">
                    <img src={add} alt="Agregar Titular" />
                  </label>
                  <h6>Agregar Titular</h6>
                </button>
              </div>
              {/* <div className="div__add">
                <button
                  id="btnAdd"
                  className="btn_none"
                  onClick={this.props.handleCreate}
                />
                <label htmlFor="btnAdd" className="lbl__add">
                  <img src={add} alt="Agregar Movimiento" />
                </label>
                <h6>Agregar cuenta</h6>
              </div> */}
            </a>

            <table className="table table-striped table-inverse">
              <thead className="thead-inverse">
                <tr>
                  <th>Nombre</th>
                  <th>RUT</th>
                  <th>Tipo</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                {data.map((accountOwner) => {
                  var textName;
                  var accountOwnerData = {
                    id: accountOwner.id,
                    rut: accountOwner.rut,
                    typePerson: {
                      id: accountOwner.typePerson.id,
                    },
                    naturalPerson: {},
                    legalPerson: {},
                  };
                  if (accountOwner.typePerson.id === 1) {
                    accountOwnerData.naturalPerson = accountOwner.naturalPerson;
                    delete accountOwnerData["legalPerson"];
                  } else if (accountOwner.typePerson.id === 2) {
                    accountOwnerData.legalPerson = accountOwner.legalPerson;
                    delete accountOwnerData["naturalPerson"];
                  }

                  if (accountOwner.typePerson.id === 1) {
                    textName = (
                      <label>
                        {accountOwner.naturalPerson.name + " "}
                        {accountOwner.naturalPerson.lastName}
                      </label>
                    );
                  } else if (accountOwner.typePerson.id === 2) {
                    textName = (
                      <label>{accountOwner.legalPerson.businessName}</label>
                    );
                  }
                  return (
                    <tr key={accountOwner.id}>
                      <td scope="row">{textName}</td>
                      <td>
                        <label>{accountOwner.rut}</label>
                      </td>
                      <td>{accountOwner.typePerson.descriptionType}</td>
                      <td>
                        <div className="div__options">
                          <a href="#popup">
                            <button
                              onClick={this.props.handleClick.bind(
                                this,
                                accountOwnerData
                              )}
                              className="btn__option"
                              /* className="btn btn-info btn-xs" */
                            >
                              <label className="lbl__add">
                                <img src={edit} alt="Agregar Movimiento" />
                              </label>
                            </button>
                          </a>
                          <a href="#">
                            <button
                              onClick={this.props.handleDelete.bind(
                                this,
                                accountOwnerData
                              )}
                              className="btn__option"
                              /* className="btn btn-danger btn-xs" */
                            >
                              <label className="lbl__add">
                                <img src={del} alt="Agregar Movimiento" />
                              </label>
                            </button>
                          </a>
                        </div>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
     
    );
  }
}

export default TableAccountOwner;
