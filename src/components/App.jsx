import React from 'react'
import {BrowserRouter, Route, Switch} from "react-router-dom"

import AccountOwner from '../pages/AccountOwner.jsx'
import Login from '../pages/Login.jsx'
import Account from '../pages/Account.jsx'
import NavBar from './NavBar.jsx'
import PrivateRoute from './common/PrivateRoute.js'

function App(){
   // localStorage.setItem('token',"");
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Login} />
                <NavBar>
                <PrivateRoute exact path="/accountowner" component={AccountOwner} />
                <PrivateRoute exact path="/account" component={Account} />
                </NavBar>
            </Switch>
        </BrowserRouter>
    );
}

export default App;