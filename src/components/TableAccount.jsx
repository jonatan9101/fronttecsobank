import React from "react";
import { Link } from "react-router-dom";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import "./styles/TableAccount.css";
import financeImg from "./img/finance.svg";
import deleteImg from "./img/delete.svg";
import viewImg from "./img/view.svg";

class TableAccountForm extends React.Component {
  handleClick = () => {
    console.log("Entro a click");
  };

  render() {
    return (
      
      <div className="table__data">
        <table className="table table-striped table-inverse">
          <thead className="thead-inverse">
            <tr>
              <th>Número de cuenta</th>
              <th>Moneda</th>
              <th>Saldo</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {this.props.accountsList.map((account) => {
              var auxButton = <div className="add__a" />;
              if (account.movements.length === 0) {
                auxButton = (
                  <a href="#" className="add__a">
                    <button
                      id="btnDelete"
                      className="btn_none"
                    />
                    <label htmlFor="btnDelete" className="add__label" onClick={this.props.handleClickDelete.bind(this, account)}>
                      <img src={deleteImg} alt="Agregar Movimiento" 
                       />
                    </label>
                  </a>
                );
              } else {
                auxButton = (
                  <a href="#popup" className="add__a">
                    <button
                      id="btnView"
                      className="btn_none"
                     
                    />
                    <label htmlFor="btnView" className="add__label" 
                     onClick={this.props.handleClickView.bind(
                        this,
                        account.id
                      )}>
                      <img src={viewImg} alt="Ver Movimientos" />
                    </label>
                  </a>
                );
              }
              return (
                <tr key={account.id}>
                  <td>{account.accountNumber}</td>
                  <td>{account.currency.currency}</td>
                  <td>{account.balance}</td>
                  <td className="container__td">
                    <div className="container__flex">
                      <a href="#popup" className="add__a">
                        <button id="btnMovement" className="btn_none"></button>
                        <label
                          htmlFor="btnAddMovement"
                          className="add__label"
                          onClick={this.props.handleClickAddMovement.bind(this, account)}
                        >
                          <img src={financeImg} alt="Agregar Movimiento" />
                        </label>
                      </a>
                      {auxButton}
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    
    );
  }
}
export default TableAccountForm;
