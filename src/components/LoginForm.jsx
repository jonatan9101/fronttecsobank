import React from "react";
import { Link } from "react-router-dom";

import "./styles/LoginForm.css";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";

class LoginForm extends React.Component {
  render() {
    return (
      
        <div className="login">
          <div className="login__form">
            <h2>Login</h2>
            <ul>
              <li>
                <input
                  type="text"
                  id="txt-email"
                  placeholder="Ingrese su usuario"
                  name="username"
                  defaultValue={this.props.user.username}
                  onChange={this.props.handleChange}
                />
              </li>
              <li>
                <input
                  type="password"
                  id="txt-password"
                  placeholder="Ingrese su contraseña"
                  name="password"
                  defaultValue={this.props.user.password}
                  onChange={this.props.handleChange}
                />
              </li>
            </ul>

            <button
              //type="button"
              className="btn"
              onClick={this.props.handleClickSubmit}
              type="button"
            >
              Ingresar
            </button>
            {this.props.error && <p className="form-control is-invalid">{this.props.error.message}</p>}
          </div>
        </div>
      
    );
  }
}
export default LoginForm;
