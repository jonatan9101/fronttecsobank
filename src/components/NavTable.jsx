import React from "react";

import PopUp from "../components/PopUp.jsx";

import add from "./img/plus.svg";

import api from "../api.js";
import "./styles/NavTable.css";

import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";

class NavTable extends React.Component {
  state = {
    currencyList: [],
    account: {
      accountNumber: "",
      currency: {
        id: "",
      },
      balance: "",
    },
    popup: undefined,
  };

  handleSubmit = async (e) => {
    e.preventDefault();
  };

  handleChange = (e) => {
    if (e.target.name == "currency") {
      this.state.account.currency.id = e.target.value;
    } else {
      this.state.account[e.target.name] = e.target.value;
      console.log(this.state.account);
    }
  };

  handleClick = async (e) => {
    const data = await api.Account.create(this.state.account);

    if (data.status == 201) {
      alert("Cuenta creada correctamente");
      this.setState({
        account: {
          accountNumber: "",
          currency: {
            id: "",
          },
          balance: "",
        },
        popup: undefined,
      });
      this.props.onCreate();
    } else {
      alert(JSON.parse(data.response).message);
    }
  };

  getCurrencies = async () => {
    this.setState({
      loading: true,
      error: null,
    });
    try {
      const data = (await api.Type.currency());
      
        this.state.currencyList= JSON.parse(data.response);
      
      console.log(this.state.currencyList)
    } catch (ex) {
      console.log(ex)
      this.setState({
        error: ex,
        loading: false,
      });
    }
  };

  createPopup = async () => {
    await this.getCurrencies();
    console.log(this.state.currencyList)
        this.setState({
      popup: {
        popupTitle: "Registrar cuenta",
        popupContent: (
          <div className="modal-body">
            <ValidatorForm onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label>Número de cuenta</label>
                <input
                  type="number"
                  onChange={this.handleChange}
                  className="form-control"
                  name="accountNumber"
                  defaultValue={this.state.account.accountNumber}
                />
              </div>
              <div className="form-group">
                <label>Tipo de moneda</label>
                <select
                  name="currency"
                  className="form-control"
                  onChange={this.handleChange}
                >
                   {this.state.currencyList.map((currency) => {
                     
                      return (<option value={currency.id}>{currency.currency}</option>);
                   })}          
                  
                </select>
              </div>
              <div className="form-group">
                <label>Valor de apertura</label>
                <input
                  type="number"
                  onChange={this.handleChange}
                  className="form-control"
                  name="balance"
                  defaultValue={this.state.account.balance}
                />
              </div>
              <div className="modal-footer justify-content-center">
                <button
                  onClick={this.handleClick}
                  type="submit"
                  className="btn btn-primary mr-1"
                >
                  Guardar
                </button>
                <button type="submit" className="btn btn-outline-secondary">
                  Cancel
                </button>
              </div>
            </ValidatorForm>
          </div>
        ),
      },
    });

    console.log(this.state.popup);
  };

  render() {
    return (
      <React.Fragment>
        <a href="#popup" className="a__add" onClick={this.createPopup}>
          <div className="div__add">
            <button
              id="btnAdd"
              className="btn_none"
              onClick={this.createPopup}
            />
            <label htmlFor="btnAdd" className="lbl__add">
              <img src={add} alt="Agregar Movimiento" />
            </label>
            <h6>Agregar cuenta</h6>
          </div>
        </a>
        <PopUp content={this.state.popup} />
      </React.Fragment>
    );
  }
}

export default NavTable;
