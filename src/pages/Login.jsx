import React from "react";
import api from "../api.js";
import LoginForm from "../components/LoginForm.jsx";
import { Redirect } from "react-router-dom";

class Login extends React.Component {
  state = {
    submitted: false,
    user: {
      username: "",
      password: "",
    },
    error: null,
    redirect: null 
  };
  render() {
    if (this.state.redirect) {
        return <Redirect to={this.state.redirect} />
      }
    return (
      <LoginForm
        onSubmit={this.handleSubmit}
        user={this.state.user}
        handleClickSubmit={this.handleClickSubmit}
        handleChange={this.handleChange}
        error={this.state.error}
      ></LoginForm>
    );
  }

  handleChange = (e) => {
    this.state.user[e.target.name] = e.target.value;
    console.log(this.state.user);
  };

  handleClickSubmit = async (e) => {
    console.log("Antes");
    if (this.state.user.username.length === 0) {
      this.setState({
        error: {
          message: "Nombre de usuario es obligatorio",
        },
      });
    } else if (this.state.user.password.length === 0) {
      this.setState({
        error: {
          message: "Contraseña es obligatoria",
        },
      });
    } else {
      this.setState({
        error: null,
      });
      this.authenticate();
    }
  };

  authenticate = async () => {
    const response = await api.User.authenticate(this.state.user);
    if (response.status == 200) {
      let token=JSON.parse(response.response).token;
      localStorage.setItem("token",token );
      //setAuthToken(token);
      console.log(this.state.user.username);
      this.setState({ redirect: "/accountowner" });
    } else if (response.status == 401) {
      this.setState({
        error: {
          message: "Usuario no está autorizado"
        },
      });   
     
    }
  };

  handleSubmit = async (e) => {
    this.setState(
      {
        submitted: true,
      },
      () => {
        setTimeout(() => {
          this.setState({ submitted: false });
        }, 5000);
      }
    );
    e.prevenDefault();
  };
}
export default Login;
