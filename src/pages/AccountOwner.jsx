import React from "react";

import api from "../api.js";
import TableAccountOwner from "../components/TableAccountOwner.jsx";
import PopUp from "../components/PopUp.jsx";
import $ from "jquery";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";

class AccountOwner extends React.Component {
  state = {
    loading: true,
    accountOwnersList: undefined,
    error: null,
    popup: {},
    accountOwner: {
      rut: "",
      typePerson: {
        id: "",
      },
      naturalPerson: {
        name: "",
        lastName: "",
        dni: "",
      },
      legalPerson: {
        businessName: "",
        foundationDate: "",
      },
    },
    newAccountOwner: {
      rut: "",
      typePerson: {
        id: "",
      },
      naturalPerson: {
        name: "",
        lastName: "",
        dni: "",
      },
      legalPerson: {
        businessName: "",
        foundationDate: "",
      },
    },
    submitted: false,
  };

  form_group = null;
  personTypeList = [];

  handleClickClear = () => {
    this.setState({
      popup: {},
      accountOwner: {
        rut: "",
        typePerson: {
          id: "",
        },
        naturalPerson: {
          name: "",
          lastName: "",
          dni: "",
        },
        legalPerson: {
          businessName: "",
          foundationDate: "",
        },
      },
      newAccountOwner: {
        rut: "",
        typePerson: {
          id: "",
        },
        naturalPerson: {
          name: "",
          lastName: "",
          dni: "",
        },
        legalPerson: {
          businessName: "",
          foundationDate: "",
        },
      },
    });
  };

  componentDidMount() {
    this.getAccountOwners();
    this.getTypesPerson();
  }

  getTypesPerson = async () => {
    this.setState({
      loading: true,
      error: null,
    });
    try {
      this.personTypeList = JSON.parse(
        await (await api.Type.person()).response
      );
    } catch (ex) {
      this.setState({
        error: ex,
        loading: false,
      });
    }
  };

  getAccountOwners = async () => {
    this.setState({
      loading: true,
      error: null,
    });
    try {
      const data = await api.AccountOwner.list();

      this.setState({
        loading: false,
        accountOwnersList: data.response,
      });
    } catch (ex) {
      this.setState({
        error: ex,
        loading: false,
      });
    }
  };

  handleChange = (e) => {
    if (e.target.name == "name") {
      this.state.accountOwner.naturalPerson.name = e.target.value;
    } else if (e.target.name == "lastName") {
      this.state.accountOwner.naturalPerson.lastName = e.target.value;
    } else if (e.target.name == "dni") {
      this.state.accountOwner.naturalPerson.dni = e.target.value;
    } else if (e.target.name == "businessName") {
      this.state.accountOwner.legalPerson.businessName = e.target.value;
    } else if (e.target.name == "foundationDate") {
      this.state.accountOwner.legalPerson.foundationDate = e.target.value;
    }
    console.log(this.state.accountOwner);
  };

  handleClickUpdate = async (e) => {
    let check = true;
    try {
      if (this.state.accountOwner.typePerson.id == 1) {
        delete this.state.accountOwner["legalPerson"];
        if (this.state.accountOwner.naturalPerson.name.length == 0) {
          alert("El nombre es requerido");
          check = false;
        } else if (this.state.accountOwner.naturalPerson.lastName.length == 0) {
          check = false;
          alert("El apellido es requerido");
        } else if (this.state.accountOwner.naturalPerson.dni.length == 0) {
          alert("La cedula es requerida");
          check = false;
        }
      } else if (this.state.accountOwner.typePerson.id == 2) {
        delete this.state.accountOwner["naturalPerson"];
        if (this.state.accountOwner.legalPerson.businessName.length == 0) {
          alert("La razon social es requerida");
          check = false;
        } else if (
          this.state.accountOwner.legalPerson.foundationDate.length == 0
        ) {
          alert("La fecha de fundación es requerida");
          check = false;
        }
      }
      console.log("Validé");
      if (check) {
        const data = await api.AccountOwner.update(this.state.accountOwner);
        if (data.status == 302) {
          try {
            delete this.state.accountOwner["legalPerson"];
            delete this.state.accountOwner["naturalPerson"];
            this.handleClickClear();
          } catch (ex) {}

          this.getAccountOwners();
        } else {
          this.setState({
            error: JSON.parse(data.response).message,
          });
          alert(this.state.error);
        }
      }
    } catch (ex) {
      this.setState({
        error: ex,
        loading: false,
      });
    }
  };

  handleClick = (accountOwner, e) => {
    if (accountOwner.typePerson.id === 1) {
      this.form_group = (
        <div>
          <label>Nombre</label>
          <input
            type="text"
            onChange={this.handleChange}
            className="form-control"
            name="name"
            defaultValue={accountOwner.naturalPerson.name}
          />
          <br />
          <label>Apellidos</label>
          <input
            type="text"
            onChange={this.handleChange}
            class="form-control"
            name="lastName"
            defaultValue={accountOwner.naturalPerson.lastName}
          />
          <br />
          <label>Cédula</label>
          <input
            type="text"
            onChange={this.handleChange}
            class="form-control"
            name="dni"
            defaultValue={accountOwner.naturalPerson.dni}
          />
        </div>
      );
    } else if (accountOwner.typePerson.id === 2) {
      this.form_group = (
        <div>
          <label>Razón Social</label>
          <input
            type="text"
            onChange={this.handleChange}
            class="form-control"
            name="businessName"
            defaultValue={accountOwner.legalPerson.businessName}
          />
          <br />
          <label>Año de fundación</label>
          <input
            type="date"
            onChange={this.handleChange}
            class="form-control"
            name="foundationDate"
            defaultValue={accountOwner.legalPerson.foundationDate}
          />
          <br />
        </div>
      );
    }
    this.state.accountOwner = accountOwner;

    var popupContent = (
      <div class="modal-body">
        <ValidatorForm onSubmit={this.handleSubmit}>
          <div class="from-group mb-2">
            <div class="form-group">
              <label>RUT</label>
              <TextValidator
                type="text"
                formControlName="rut"
                class="form-control"
                value={accountOwner.rut}
                validators={["required"]}
                errorMessages={["El rut es obligatorio"]}
                disabled
              />
              <div class="invalid-feedback">
                <div>El RUT es rquerido</div>
              </div>
            </div>
            <div class="form-group">
              <label>Tipo de titular</label>
              {this.personTypeList.map((typePerson) => {
                if (accountOwner.typePerson.id == typePerson.id) {
                  return (
                    <div class="row">
                      <div class="col-md-2">
                        <input
                          type="radio"
                          name="typePerson"
                          formControlName="typePerson"
                          class="form-control"
                          value={typePerson.id}
                          checked
                          disabled
                        />
                      </div>
                      <div class="col-md-4">
                        <label>{typePerson.descriptionType}</label>
                      </div>
                    </div>
                  );
                } else {
                  return (
                    <div class="row">
                      <div class="col-md-2">
                        <input
                          type="radio"
                          name="typePerson"
                          formControlName="typePerson"
                          class="form-control"
                          value={typePerson.id}
                          disabled
                        />
                      </div>
                      <div class="col-md-4">
                        <label>{typePerson.descriptionType}</label>
                      </div>
                    </div>
                  );
                }
              })}
            </div>
            <div class="form-group">{this.form_group}</div>
          </div>
          <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-outline-secondary">
              Cancel
            </button>
            <button
              onClick={this.handleClickUpdate}
              type="submit"
              class="btn btn-primary mr-1"
            >
              Guardar
            </button>
          </div>
        </ValidatorForm>
      </div>
    );

    this.setState({
      popup: {
        uid: "",
        popupTitle: "Registro de titular",
        popupContent: popupContent,
      },
    });
  };

  handleDelete = async (accountOwner, e) => {
    try {
      const data = await api.AccountOwner.delete(accountOwner.id);
      if (data.status == 204) {
        this.getAccountOwners();
      }
      this.setState({});
    } catch (ex) {
      this.setState({
        error: ex,
        loading: false,
      });
    }
  };

  handleChangeType = (typePerson, e) => {
    if (typePerson.id == 1) {
      document.getElementById("naturalPersonDiv").removeAttribute("hidden");
      document.getElementById("legalPersonDiv").setAttribute("hidden", true);
    } else if (typePerson.id == 2) {
      document.getElementById("legalPersonDiv").removeAttribute("hidden");
      document.getElementById("naturalPersonDiv").setAttribute("hidden", true);
    }
    this.state.newAccountOwner.typePerson.id = typePerson.id;
  };

  handleChageCreate = (e) => {
    console.log(e.target.name+": "+e.target.value);
    if (e.target.name == "rut") {
      this.state.newAccountOwner.rut = e.target.value;
    } else if (e.target.name == "name") {
      this.state.newAccountOwner.naturalPerson.name = e.target.value;
    } else if (e.target.name == "lastName") {
      this.state.newAccountOwner.naturalPerson.lastName = e.target.value;
    } else if (e.target.name == "dni") {
      this.state.newAccountOwner.naturalPerson.dni = e.target.value;
    } else if (e.target.name == "businessName") {
      this.state.newAccountOwner.legalPerson.businessName = e.target.value;
    } else if (e.target.name == "foundationDate") {
      this.state.newAccountOwner.legalPerson.foundationDate = e.target.value;
    }
    console.log(this.state.newAccountOwner);
  };

  handleClickCreate = async (e) => {
    console.log(this.state.newAccountOwner);
    this.setState({ submitted: true }, () => {
      setTimeout(() => this.setState({ submitted: false }), 5000);
    });
    
    if (this.state.submitted) {
      try {
        if (this.state.newAccountOwner.typePerson.id == 1) {
          delete this.state.newAccountOwner["legalPerson"];
        } else if (this.state.newAccountOwner.typePerson.id == 2) {
          delete this.state.newAccountOwner["naturalPerson"];
        }
        const data = await api.AccountOwner.create(this.state.newAccountOwner);
        if (data.status == 201) {
          delete this.state["naturalPerson"];
          this.setState({
            newAccountOwner: {
              rut: "",
              typePerson: {
                id: "",
              },
              naturalPerson: {
                name: "",
                lastName: "",
                dni: "",
              },
              legalPerson: {
                businessName: "",
                foundationDate: "",
              },
            },
          });
          this.getAccountOwners();
        } else {
          this.setState({
            error: JSON.parse(data.response).message,
          });
          alert(this.state.error);
        }
        this.setState({});
      } catch (ex) {
        this.setState({
          error: ex,
          loading: false,
        });
      }
    }
  };

  handleSubmit = async (e) => {
    e.preventDefault();
  };

  handleCreate = async (e) => {
    var popupContent = (
      <div class="modal-body">
        <ValidatorForm onSubmit={this.handleSubmit} onError={errors => console.log(errors)}>
          <div class="form-group mb-2">
            <div class="form-group">
              <label>RUT</label>
              <TextValidator
                onChange={this.handleChageCreate}
                type="text"
                class="form-control"
                name="rut"
                validators={["required"]}
                errorMessages={["El rut es obligatorio"]}
                defaultValue={this.state.newAccountOwner.rut}
              />
           </div>
            <div class="form-group">
              <label>Tipo de titular</label>
              {this.personTypeList.map((typePerson) => {
                return (
                  <div class="row">
                    <div class="col-md-2">
                      <input
                        onChange={this.handleChangeType.bind(this, typePerson)}
                        type="radio"
                        name="typePerson"
                        formControlName="typePerson"
                        class="form-control"
                        value={typePerson.id}
                      />
                    </div>
                    <div class="col-md-4">
                      <label>{typePerson.descriptionType}</label>
                    </div>
                  </div>
                );
              })}
            </div>
            <div id="naturalPersonDiv" hidden>
              <label>Nombre</label>
              <input
                type="text"
                className="form-control"
                name="name"
                defaultValue={this.state.newAccountOwner.naturalPerson.name}
                onChange={this.handleChageCreate}
              />
              <br />
              <label>Apellidos</label>
              <input
                type="text"
                class="form-control"
                name="lastName"
                defaultValue={this.state.newAccountOwner.naturalPerson.lastName}
                onChange={this.handleChageCreate}
              />
              <br />
              <label>Cédula</label>
              <input
                type="text"
                class="form-control"
                name="dni"
                defaultValue={this.state.newAccountOwner.naturalPerson.dni}
                onChange={this.handleChageCreate}
              />
            </div>
            <div id="legalPersonDiv" hidden>
              <label>Razón Social</label>
              <input
                type="text"
                class="form-control"
                name="businessName"
                defaultValue={
                  this.state.newAccountOwner.legalPerson.businessName
                }
                onChange={this.handleChageCreate}
              />
              <br />
              <label>Año de fundación</label>
              <input
                type="date"
                class="form-control"
                name="foundationDate"
                defaultValue={
                  this.state.newAccountOwner.legalPerson.foundationDate
                }
                onChange={this.handleChageCreate}
              />
              <br />
            </div>
          </div>
          {this.state.error && <div>{this.state.error}</div>}
          <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-outline-secondary">
              Cancel
            </button>
            <button
              onClick={this.handleClickCreate}
              type="submit"
              class="btn btn-primary mr-1"
            >
              Crear
            </button>
          </div>
        </ValidatorForm>
      </div>
    );

    this.setState({
      popup: {
        popupTitle: "Registro de titular",
        popupContent: popupContent,
      },
    });
  };

  render() {
    if (this.state.loading === true) {
      return (
        <center>
          <p>Cargando...</p>
        </center>
      );
    }
    return (
      <React.Fragment>
       
        <div className="table__data">
          <h1>Gestión de titulares</h1>
        </div>
        <TableAccountOwner
          accountOwnersList={this.state.accountOwnersList}
          handleClick={this.handleClick}
          handleDelete={this.handleDelete}
          handleCreate={this.handleCreate}
        />
        <PopUp
          handleClickClear={this.handleClickClear}
          content={this.state.popup}
        />
      </React.Fragment>
    );
  }
}

export default AccountOwner;
