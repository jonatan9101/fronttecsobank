import React from "react";
import api from "../api.js";
import PopUp from "../components/PopUp.jsx";
import NavTable from "../components/NavTable.jsx";


import AccountForm from "../components/AccountForm.jsx";

import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";

class Account extends React.Component {
  state = {
    submitted: false,
    loading: false,
    accountList: [
      /*
      {
        id: 1,
        accountNumber: 125654544,
        currency: {
          id: 1,
          currency: "Pesos",
          shortName: "COP",
          minBalance: 1000,
        },
        balance: 2000,
        movements: [
          {
            id: 1,
            movementDate: "",
            description: "nómina",
            amount: 25456,
            movementType: {
              id: 1,
              descriptionType: "Débito",
            },
          },
          {
            id: 1,
            movementDate: "",
            description: "Retiro",
            amount: 25456,
            movementType: {
              id: 2,
              descriptionType: "Crédito",
            },
          },
        ],
      },
      {
        id: 2,
        accountNumber: 125654545,
        currency: {
          id: 1,
          currency: "Dólar",
          shortName: "US",
          minBalance: 500,
        },
        balance: 5214,
        movements: [],
      },
    */
    ],
    movementTypelist: [],
    account: {
      id: null,
      accountNumber: null,
      currency: {
        id: null,
      },
      balance: null,
    },
    newAccount: {
      id: null,
      accountNumber: null,
      currency: {
        id: null,
      },
      balance: null,
    },
    newMovement: {
      amount: null,
      movementType: {
        id: null,
      },
      movementDate: null,
      description: null,
      account: {},
    },
    movementsList: [],
    popup: undefined,
  };

  componentDidMount() {
    this.getAccounts();
    this.getMovementTypes();
  }

  getMovementTypes = async () => {
    this.setState({
      loading: true,
      error: null,
    });
    try {
      const data = await api.Type.movementType();

      this.state.movementTypelist = JSON.parse(data.response);
    } catch (ex) {
      this.setState({
        error: ex,
        loading: false,
      });
    }
  };

  getAccounts = async () => {
    this.setState({
      loading: true,
      error: null,
    });
    try {
      this.setState({
        loading: false,
        accountList: JSON.parse(await (await api.Account.list()).response),
      });

      console.log(this.state.accountList);
    } catch (ex) {
      this.setState({
        error: ex,
        loading: false,
      });
    }
  };

  getMovements = async (id) => {
    this.setState({
      loading: true,
      error: null,
    });
    try {
      this.setState({
        loading: false,
        movementsList: JSON.parse(await (await api.Movement.list(id)).response),
      });

      console.log(this.state.movementsList);
    } catch (ex) {
      this.setState({
        error: ex,
        loading: false,
      });
    }
  };

  handleChangeMovement = (e) => {
    console.log(e.target);
    if (e.target.name == "movementType") {
      this.state.newMovement.movementType.id = e.target.value;
    } else {
      this.state.newMovement[e.target.name] = e.target.value;
    }
    console.log(this.state.newMovement);
  };

  handleClickMovement = async (e) => {
    this.setState({ submitted: true }, () => {
      setTimeout(() => this.setState({ submitted: false }), 5000);
    });
    //this.state.newMovement.account = account;
    if (this.state.newMovement.movementType.id == null) {
      return alert("Tipo de movimiento es obligatorio");
    } else {
      this.state.newMovement.movementDate = new Date();
      const data = await api.Movement.create(this.state.newMovement);
      if (data.status == 201) {
        alert("Movimiento agregado correctamente");
      } else {
        alert(data.response);
      }
      console.log(this.state.newMovement);
    }
    this.handleClickClear();
    this.getAccounts();
  };

  handleClickClear = () => {
    this.setState({
      popup: undefined,
      newMovement: {
        amount: null,
        movementType: {
          id: null,
        },
        movementDate: null,
        description: null,
      },
    });
  };

  handleClickDelete = async (account, e) => {
    console.log("Eliminar" + account.accountNumber);
    try {
      const data = await api.Account.delete(account.id);
      if (data.status == 204) {
        this.getAccounts();
      }
      this.setState({});
    } catch (ex) {
      this.setState({
        error: ex,
        loading: false,
      });
    }
  };

  handleSubmit = async (e) => {
    e.preventDefault();
  };

  handleClickView = async (id, e) => {
    await this.getMovements(id);
    console.log(this.state.movementsList);
    var popupContent = (
      <div className="modal-body">
        <ValidatorForm onSubmit={this.props.handleSubmit}>
          <div className="from-group mb-2">
            <div className="form-group">
              <label>Movimientos de cuenta</label>
              <div className="table__data">
                <table className="table table-striped table-inverse">
                  <thead className="thead-inverse">
                    <tr>
                      <th>Descripción</th>
                      <th>Tipo de movimiento</th>
                      <th>Fecha</th>
                      <th>Saldo</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.movementsList.map((movement) => {
                      //console.log(account);
                      var date = new Date(movement.movementDate);
                      return (
                        <tr key={movement.id}>
                          <td>{movement.description}</td>
                          <td>{movement.movementType.descriptionType}</td>
                          <td>{date.toLocaleString("es-ES")}</td>
                          <td>{movement.amount}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>

            <div className="form-group">{this.form_group}</div>
          </div>
          <div className="modal-footer justify-content-center">
            <button type="submit" className="btn btn-outline-secondary">
              Cerrar
            </button>
          </div>
        </ValidatorForm>
      </div>
    );
    this.setState({
      popup: {
        size: "Big",
        popupTitle: "Ver Movimientos",
        popupContent: popupContent,
      },
    });
  };

  handleClickAddMovement = (account, e) => {
    console.log("Voy a agregar");
    console.log();
    //var account=e.target.id;
    console.log(account);
    //this.state.newMovement.acount=account;

    this.state.newMovement.account = account;
    console.log(this.state.newMovement);
    var popupContent = (
      <div className="modal-body">
        <ValidatorForm onSubmit={this.props.handleSubmit}>
          <div className="from-group mb-2">
            <div className="form-group">
              <label>Número de cuenta</label>
              <input
                type="number"
                onChange={this.handleChangeMovement}
                className="form-control"
                defaultValue={this.state.newMovement.account.accountNumber}
                disabled
              />
            </div>
            <div className="form-group">
              <label>Tipo</label>
              {this.state.movementTypelist.map((movementType) => {
                return (
                  <div className="row" key={movementType.id}>
                    <div className="col-md-2">
                      <input
                        type="radio"
                        name="movementType"
                        className="form-control"
                        defaultValue={movementType.id}
                        onChange={this.handleChangeMovement}
                      />
                    </div>
                    <div className="col-md-4">
                      <label>{movementType.descriptionType}</label>
                    </div>
                  </div>
                );
              })}
            </div>
            <div className="form-group">
              <label>Descripción</label>
              <TextValidator
                type="text"
                className="form-control"
                name="description"
                defaultValue={this.state.newMovement.description}
                validators={["required"]}
                errorMessages={["La descripción es obligatoria"]}
                onChange={this.handleChangeMovement}
              />
            </div>
            <div className="form-group">
              <label>Valor</label>
              <TextValidator
                type="number"
                name="amount"
                className="form-control"
                validators={["required"]}
                errorMessages={["El valor es obligatorio"]}
                defaultValue={this.state.newMovement.amount}
                onChange={this.handleChangeMovement}
              />
            </div>

            <div className="form-group">{this.form_group}</div>
          </div>
          <div className="modal-footer justify-content-center">
            <button type="submit" className="btn btn-outline-secondary">
              Cancel
            </button>
            <button
              onClick={this.handleClickMovement}
              type="submit"
              className="btn btn-primary mr-1"
            >
              Guardar
            </button>
          </div>
        </ValidatorForm>
      </div>
    );

    /* this.setState({
      popup: {
        uid: "",
        popupTitle: "Registrar movimiento",
        popupContent: popupContent,
      },
    }); */
    this.createAddMovement(popupContent);
  };

  handleClickCreate = async (e) => {
    this.getAccounts();
  };

  createAddMovement = (popupContent) => {
    this.setState({
      popup: {
        popupTitle: "Registrar Movimiento",
        popupContent: popupContent,
      },
    });
  };

  render() {
    if (this.state.accountList.length == 0) {
      return (
        <React.Fragment>
          <NavTable onCreate={this.handleClickCreate} />
          <h2>Crea el primer movimiento</h2>
          <PopUp
            handleClickClear={this.handleClickClear}
            content={this.state.popup}
          />
        </React.Fragment>
      );
    }

    return (
      <React.Fragment>
       
      <div className="table__data">
        <h1>Gestión de cuentas</h1>
      </div>
        <NavTable onCreate={this.handleClickCreate} />
        <AccountForm
          accountsList={this.state.accountList}
          handleClickAddMovement={this.handleClickAddMovement}
          handleClickDelete={this.handleClickDelete}
          movementTypelist={this.state.movementTypelist}
          createAddMovement={this.createAddMovement}
          newMovement={this.state.newMovement}
          handleClickMovement={this.handleClickMovement}
          handleClickView={this.handleClickView}
        ></AccountForm>
        <PopUp
          handleClickClear={this.handleClickClear}
          content={this.state.popup}
        />
      </React.Fragment>
    );
  }
}
export default Account;
