import React from 'react';
import ReactDOM from 'react-dom';

import App from "./components/App.jsx";
import './index.css'

const container = document.getElementById("app");
ReactDOM.render(<App/>, container)
