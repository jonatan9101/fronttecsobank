import decode from "jwt-decode";

const BASE_URL = "http://localhost:7777/api/";

const TOKEN = "";

async function callApi(endpoint, type, data = null) {
  try {
    console.log(data);
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = false;

    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        return this;
      }
    });

    xhr.open(type, BASE_URL + endpoint, false);
    if (endpoint != "authenticate") {
      console.log("Inicando api.."+localStorage.getItem("token"));
      xhr.setRequestHeader(
        "Authorization",
        "Bearer " + localStorage.getItem("token")
      );
    }
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(data));
    return xhr;
  } catch (ex) {
    return ex;
  }
}




const api = {
  AccountOwner: {
    list() {
      return callApi("accountowner", "GET");
    },
    create(accountOwner) {
      return callApi("accountowner", "POST", accountOwner);
    },
    update(accountOwner) {
      return callApi("accountowner", "PUT", accountOwner);
    },
    delete(id) {
      return callApi("accountowner/" + id, "DELETE");
    },
  },
  Type: {
    person() {
      return callApi("type/person", "GET");
    },
    movementType() {
      return callApi("type/movement", "GET");
    },
    currency() {
      return callApi("type/currency", "GET");
    }
  },
  User: {
    authenticate(user) {
      return callApi("authenticate", "POST", user);
    },
  },
  Account: {
    list() {
      return callApi("account", "GET");
    },
    create(account){
      return callApi("account", "POST", account)
    },
    delete(id){
      return callApi("account/"+id, "DELETE")
    }
  },
  Movement: {
    list(id){
      return callApi("account/movement/byAccount/"+id, "GET");
    },
    create(movement) {
      return callApi("account/movement", "POST", movement);
    },
  },
 
};




export default api;
